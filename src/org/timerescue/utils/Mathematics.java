package org.timerescue.utils;

import org.timerescue.element.Coordinate;

/**
 * This class provides some general purpose services 
 * that do mathematical operation in order to get the output 
 * @author chamanx
 *
 */
public final class Mathematics {
	
	/**
	 * 
	 * @return a double with the distance between the 2 coordinate objects
	 */
	public static double DistanceBetweenPoints2D( Coordinate p1, Coordinate p2)
	throws NullPointerException{
		
		double distance = Math.pow(
				Math.pow(p1.getX() - p2.getX(), 2) + 
				Math.pow(p1.getY() - p2.getY(), 2), 0.5);
		return distance;
		
	}
	
	/**
	 * 
	 * @return a double with the distance between the 2 coordinate objects
	 */
	public static double DistanceBetweenPoints3D( Coordinate p1, Coordinate p2)
	throws NullPointerException{
		
		double distance = Math.pow(
				Math.pow(p1.getX() - p2.getX(), 2) + 
				Math.pow(p1.getY() - p2.getY(), 2) +
				Math.pow(p1.getZ() - p2.getZ(), 2), 0.5);
		return distance;
		
	}
}
