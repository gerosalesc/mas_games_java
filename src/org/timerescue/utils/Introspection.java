package org.timerescue.utils;

import org.timerescue.information.Serializable;

/**
 * This class is meant to public some common services that may be used for the project. 
 * The services here use Java Introspection features 
 * @author chamanx
 *
 */
public class Introspection {
	
	/*
	 * Methods
	 */
	
	/**
	 * It receives a class name and return a Serializable object of that class
	 * @param class_name name of the class
	 * @return the Serializable object of the specified class
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	public static Serializable getSerializableFromClass(String class_name) 
			throws ClassNotFoundException, 
			InstantiationException, 
			IllegalAccessException {
		
		Class<Serializable> cls = null;
		Serializable object = null;
		cls = ((Class<Serializable>) Class.forName(class_name));
		object = (Serializable) cls.newInstance();		
		return object;
	}
}
