/**
 * 
 */
package org.timerescue.element;

import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.Serializable;
import org.timerescue.utils.Mathematics;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;


/**
 * @author chamanx
 *
 */
public class Coordinate implements Serializable{

	/**
	 * Atribs
	 */
	private int x;
	private int y;
	private int z;
	
	
	/**
	 * Methods
	 */
	
	/**
	 * Default constructor sets x and y to 0
	 */
	public Coordinate() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	/**
	 * Sets x and y with the values passed as parameters
	 * @param x
	 * @param y
	 */
	public Coordinate(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * @return the z
	 */
	public int getZ() {
		return z;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(int z) {
		this.z = z;
	}
	
	/**
	 * Tell if a given coordinate 'neighbor' is close enough
	 * @param neighbor is the coordinate  to be checked
	 * @param radio is the radial distance to be considered the maximum value being close  
	 * @return true if close or false if false
	 */
	public boolean isClose(Coordinate neighbor, int radio) {
		// TODO Auto-generated method stub
		boolean answer = false;
		double distance = 0;
		radio = Math.abs(radio);
		//Distance between the points in a 2D map is calculated
		distance = Mathematics.DistanceBetweenPoints2D(neighbor, this);
		Mathematics.DistanceBetweenPoints3D(neighbor, this);
		//If the distance is higher than the radio then it's a neighbor 
		if(distance > radio){
			answer = true;
		}
		return answer;
	}
	
	@Override
	public String toSerial() {
		JsonObject serializer = new JsonObject();
		serializer.addProperty(Constants.X_PROPERTY, x);
		serializer.addProperty(Constants.Y_PROPERTY, y);
		serializer.addProperty(Constants.Z_PROPERTY, z);
		return gson.toJson(serializer);
	}

	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		try {
			JsonObject deserializer = parser.parse(serialized).getAsJsonObject();
			x = deserializer.get(Constants.X_PROPERTY).getAsInt();
			y = deserializer.get(Constants.Y_PROPERTY).getAsInt();
			y = deserializer.get(Constants.Z_PROPERTY).getAsInt();
		} catch (IllegalStateException e) {
			throw new InvalidSerialException(e);
		} catch (NullPointerException e) {
			throw new InvalidSerialException(e);
		} catch (JsonParseException e) {
			throw new InvalidSerialException(e);
		}	
	}
	
	/**
	 * Constants
	 * @author chamanx
	 *
	 */
	public static class Constants{
		public static final int DEFAULT_NEIGHBOR_RADIO = 5;
		public static final String X_PROPERTY = "X";
		public static final String Y_PROPERTY = "Y";
		public static final String Z_PROPERTY = "Z";
	}
	
}
