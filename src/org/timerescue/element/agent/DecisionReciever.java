/**
 * 
 */
package org.timerescue.element.agent;

import org.timerescue.action.Action;

/**
 * @author chamanx
 *
 */
public interface DecisionReciever {
	
	/**
	 * Wander for new events from player
	 */
	public Action decidePlayer(); 

}
