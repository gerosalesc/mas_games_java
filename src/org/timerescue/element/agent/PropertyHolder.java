/**
 * 
 */
package org.timerescue.element.agent;

import org.timerescue.exception.InvalidPropertyException;
import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.Serializable;
import org.timerescue.information.StringSerial;

/**
 * Managment for properties
 * @author chamanx
 *
 */
public class PropertyHolder implements Serializable{
	
	/*
	 * Attributes
	 */
	/**
	 * Properties source for the current state of the agent
	 */
	private Properties current_properties;
	/**
	 * Properties source for the initial or default state of the agent
	 */
	private Properties default_properties;
	
	/*
	 * Methods
	 */
	/**
	 * Sets current property's value
	 * @param property
	 * @param value
	 */
	public void setProperty(String property, String value) {
		current_properties.addParamData(
				property, 
				new StringSerial(value));
	}
	
	/**
	 * Get the property value from the current properties source
	 * @return current life
	 */
	public String getCurrentProperty(String property){
		return getProperty(property, this.current_properties);
	}
	
	/**
	 * Get the property value from the default properties source
	 * @return current life
	 */
	public String getDefaultProperty(String property){
		return getProperty(property, this.default_properties);
	}
	
	/**
	 * Get the property value of a given properties source object 
	 * @param property to be obtained
	 * @param props Properties object source
	 * @return
	 */
	private String getProperty(String property, Properties props){
		StringSerial value = new StringSerial();		
		try {
			value = (StringSerial) props.getParam(property);
		} catch (InvalidPropertyException e) {
			// TODO Log this
		}		
		return value.getValue();
	}
	
	/**
	 * Setter for default properties
	 * @param default_properties
	 */
	public void setDefaultProperties(Properties default_properties) {
		this.default_properties = default_properties;
	}
	
	@Override
	public String toSerial() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		// TODO Auto-generated method stub
		
	}
}
