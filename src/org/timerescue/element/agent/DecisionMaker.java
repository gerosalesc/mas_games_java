/**
 * 
 */
package org.timerescue.element.agent;

import org.timerescue.action.Action;
import org.timerescue.element.Element;
import org.timerescue.information.InformationParameters;
import org.timerescue.information.InformationParametersSerials;

/**
 * @author chamanx
 *
 */
public interface DecisionMaker {

		
	/**
	 * Wander decision for none player character
	 */
	public Action decideNonePlayer(); 
	
	/**
	 * Read the environment information object and return an element list
	 * @return
	 */
	public Element[] readSurroundings(InformationParameters information);
	
	/**
	 * Apply a taken decision as a action
	 * @param action
	 */
	public void executeDecision(Action action);
	
}
