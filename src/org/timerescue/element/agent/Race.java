package org.timerescue.element.agent;

import java.util.ArrayList;
import org.timerescue.action.Action;
import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.Serializable;

/**
 * initially a wrapper for a list of passive skills
 * @author chamanx
 *
 */
public class Race implements Serializable{
	
	/*
	 * Attributes
	 */
	private ArrayList<Action> passive_skills;

	/**
	 * @return the passive_skills
	 */
	protected Action[] getPassive_skills() {
		return (Action[]) passive_skills.toArray();
	}
	
	/*
	 * Methods 
	 */
	public Race() {
		passive_skills = new ArrayList<Action>();		
	}
	
	/**
	 * Add a new skill
	 * @param skill
	 */
	public void addSkill(Action skill) {
		passive_skills.add(skill);
	}
	
	/**
	 * clear all skills
	 */
	public void clearSkills() {
		passive_skills.clear();
	}

	@Override
	public String toSerial() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		// TODO Auto-generated method stub
		
	}
}
