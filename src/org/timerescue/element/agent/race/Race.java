package org.timerescue.element.agent.race;
/**
 * @author chamanx
 */

import java.util.Vector;

import org.timerescue.action.Action;


public class Race {
	
	/**
	 * Atribs
	 */
	/**
	 * Active race's skills. This actions should be 
	 * aviable for the agent to use
	 */
	private Vector<Action> active_skills;	
	private Integer id;
	
	/**
	 * Passive race's skills. This actions should be
	 * executed when the Agent is created.
	 */
	private Vector<Action> passive_skills;
	
	/**
	 * Static Variables
	 */
	public final Integer LUMINITY = 1;
	public final Integer HOMO_SAPIENS = 2;

}