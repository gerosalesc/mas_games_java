/**
 * 
 */
package org.timerescue.element.agent;

import java.util.Iterator;
import java.util.Vector;

import org.timerescue.action.Action;
import org.timerescue.enviroment.ActionExecuter;
import org.timerescue.exception.InvalidPropertyException;
import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.Serializable;
import org.timerescue.information.StringSerial;


/**
 * This class have current active states and consitions that are affecting the agent 
 * 
 * @author chamanx
 *
 */
public class StateHolder implements Serializable{
	
	/*
	 * Attributes
	 */
	/**
	 * Periodical, temporal Skills, states, effects
	 */
	private Vector<Action> active_states;
	/**
	 * Default, racial, environment skills, states, effects
	 */
	private Vector<Action> passive_states;
	
	
	/*
	 * Methods
	 */	
	/**
	 * Add a new state to the list
	 * @return
	 */
	public void addState(Action state){
		//check if we don't have anymore states like this one
		if(isStateActive(state.getClass())){
			//TODO Check for defensive auras of equipment effects
			active_states.add(state);
		}else{
			//TODO Renew periodical effect
		}
	}
	
	/**
	 * remove an active state from the list
	 * @return
	 */
	public void removeState(Action state){
		active_states.remove(state);
	}
	
	/**
	 * remove all states
	 * @return
	 */
	public void clearStates(){
		active_states.clear();
	}
	
	/**
	 * get a state of a given class
	 * @return null we don't have an state of that class
	 */
	public Action getState(Class<?> clas){
		for (Iterator<Action> iterator = active_states.iterator(); iterator.hasNext();) {
			Action action = iterator.next();
			if (action.getClass().equals(clas)) {
				return action;
			}
		}
		return null;
	}
	
	/**
	 * Look in all active states if there is an instance of the given Action class
	 * @param clas is the class object for an Action specification 
	 * @return true if there is one active 
	 * action of the given class for this Agent false if not
	 */
	public boolean isStateActive(Class<?> clas) {
		if(getState(clas)!= null){
			return true;
		}
		return false;
	}

	@Override
	public String toSerial() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		// TODO Auto-generated method stub
		
	}
}
