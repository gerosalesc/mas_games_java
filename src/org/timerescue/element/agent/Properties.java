/**
 * 
 */
package org.timerescue.element.agent;

import java.util.HashMap;
import java.util.List;

import org.timerescue.exception.InvalidPropertyException;
import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.InformationParameters;
import org.timerescue.information.Serializable;
import org.timerescue.information.StringSerial;

/**
 * @author chamanx
 *
 */
public class Properties implements InformationParameters{
	/*
	 * Attributes
	 */
	/**
	 * All the objects
	 */
	private HashMap<String, StringSerial> parameters;
	
	/*
	 * Methods
	 */	
	//@Inject
	/**
	 * Default constructor
	 */
	public Properties() {
		//Initialization
		this.parameters = new HashMap<String, StringSerial>();
	}
	
	/**
	 * Set the parameters
	 */
	protected void setParameters(HashMap<String, StringSerial> parameters){
		this.parameters = parameters;
	}
	
	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#getSize()
	 */
	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return parameters.size();
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#getParam(java.lang.String)
	 */
	@Override
	public Serializable getParam(String property)
			throws InvalidPropertyException {
		// TODO Auto-generated method stub
		StringSerial params;		
		params = parameters.get(property);
		return params;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#getArrayParam(java.lang.String)
	 */
	@Override
	public List<Serializable> getArrayParam(String property)
			throws InvalidPropertyException {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#addParamData(java.lang.String, org.timerescue.information.Serializable)
	 */
	@Override
	public void addParamData(String property, Serializable data_object) {
		StringSerial data = (StringSerial) data_object;
		parameters.put(property, data);
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#clear()
	 */
	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}
	
	/*
	 * (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#addArrayParamData(java.lang.String, java.util.List)
	 */
	@Override
	public void addArrayParamData(String property,
			List<Serializable> data_objects) {
		// TODO Auto-generated method stub
		
	}
	
	/* (non-Javadoc)
	 * @see org.timerescue.information.Serializable#toSerial()
	 */
	@Override
	public String toSerial() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.Serializable#fromSerial(java.lang.String)
	 */
	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		// TODO Auto-generated method stub

	}
}
