package org.timerescue.element;

import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.Serializable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * @author chamanx
 *
 */
public class Element extends Thread implements Serializable{
	
	/**
	 * Atribs
	 */
	private Coordinate coordinate;

	
	/**
	 * Methods
	 */
	
	/**
	 * 
	 */
	public Element() {
		this.coordinate = new Coordinate();
	}

	/**
	 * @return the coordinate
	 */
	public Coordinate getCoordinate() {
		return coordinate;
	}
	
	/**
	 * @param coordinate the coordinate to set
	 */
	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	
	/**
	 * Sets current coordinate with x and y values
	 * @param x
	 * @param y
	 */
	public void setCoordinate(int x, int y, int z) {
		this.coordinate = new Coordinate(x,y,z);
	}
	
	/**
	 * Move toward a direction
	 * @param vector
	 */
	public void move(Coordinate vector, int speed) {
		// TODO implement this
		
	}

	@Override
	public String toSerial() {
		JsonObject serializer = new JsonObject();
		//Serialize coordinate property
		serializer.addProperty(Constants.COORDINATE_PROPERTY, coordinate.toSerial());
		return gson.toJson(serializer);
	}

	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		try {
			JsonObject deserializer = parser.parse(serialized).getAsJsonObject();
			this.coordinate.fromSerial(
					deserializer.get(Constants.COORDINATE_PROPERTY).getAsString());
		} catch (IllegalStateException e) {
			throw new InvalidSerialException(e);
		} catch (NullPointerException e) {
			throw new InvalidSerialException(e);
		} catch (JsonParseException e) {
			throw new InvalidSerialException(e);
		}		
	}
	
	/*
	 * Constants
	 */
	public static class Constants{
		public static final String COORDINATE_PROPERTY = "COORDINATE";
	}
}
