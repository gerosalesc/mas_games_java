/**
 * 
 */
package org.timerescue.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.timerescue.action.DefaultAttackAction;
import org.timerescue.information.Serializable;
import org.timerescue.utils.Introspection;

/**
 * @author chamanx
 *
 */
public class IntrospectionTest {

	/**
	 * Test method for {@link org.timerescue.utils.Introspection#getSerializableFromClass(java.lang.String)}.
	 */
	@Test
	public void testGetSerializableFromClass() {
		DefaultAttackAction action = new DefaultAttackAction();
		String clasname = action.getClass().getName();
		Serializable ser = null;
		try {
			ser = Introspection.getSerializableFromClass(action.getClass().getName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Error ClassNotFound");
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Error InstantiationException");
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Error IllegalAccessException");
		}
		
		assertArrayEquals(clasname.getBytes(), ser.getClass().getName().getBytes() );
		
	}

}
