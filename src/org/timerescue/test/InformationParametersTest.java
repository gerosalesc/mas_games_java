/**
 * 
 */
package org.timerescue.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.timerescue.action.ExecutionContext;
import org.timerescue.element.Coordinate;
import org.timerescue.element.Element;
import org.timerescue.exception.InvalidPropertyException;
import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.InformationParametersSerials;
import org.timerescue.information.Serializable;

/**
 * @author chamanx
 *
 */
public class InformationParametersTest {

	/**
	 * Test method for {@link org.timerescue.information.InformationParametersSerials#getParam(java.lang.String)}.
	 */
	@Test
	public void testGetParam() {
		InformationParametersSerials info = new InformationParametersSerials();
		Element el1 = new Element(), el2 = new Element();
		el1.setCoordinate(1, 2);
		info.addParamData("PROP", el1);
		try {
			el2 = (Element)info.getParam("PROP");
		} catch (InvalidPropertyException e) {
			// TODO Auto-generated catch block
			fail(e.getMessage());
		}
		assertArrayEquals(el1.toSerial().getBytes(), el2.toSerial().getBytes());
	}
	
	/**
	 * Test method for {@link org.timerescue.information.InformationParametersSerials#getParamSerial(java.lang.String)}.
	 */
	@Test
	public void testGetParamSerial() {
		InformationParametersSerials param = new InformationParametersSerials();
		Coordinate cor1 = new Coordinate(1,2), cor2 = new Coordinate(3, 4);
		param.addParamData("PROP", cor1);
		try {
			cor2.fromSerial(param.getParamSerial("PROP"));
		} catch (InvalidSerialException e) {
			// TODO Auto-generated catch block
			fail(e.getMessage());
		}		
		assertArrayEquals(cor1.toSerial().getBytes(), cor2.toSerial().getBytes());
		
	}
	
	/**
	 * Test method for {@link org.timerescue.information.InformationParametersSerials#fromSerial(java.lang.String)}.
	 */
	@Test
	public void testFromSerial() {
		InformationParametersSerials ec = new InformationParametersSerials();
		InformationParametersSerials ec2 = new InformationParametersSerials();
		ec.addParamData("PROP1", new Coordinate(1,2));
		ec2.addParamData("PROP2", new Coordinate(3,4));
		try {
			String temp = ec.toSerial();
			ec2.fromSerial(temp);
		} catch (InvalidSerialException e) {
			fail(e.toString());
		}
		String temp1 = ec.toSerial();
		String temp2 = ec2.toSerial();
		assertArrayEquals(temp1.getBytes(), temp2.getBytes());
	}
	
	/**
	 * Test method for {@link org.timerescue.information.InformationParametersSerials#addArrayParamData(java.lang.String, org.timerescue.information.Serializable[])}.
	 */
	@Test
	public void testAddParamArrayData() {
		//TODO
		InformationParametersSerials info = new InformationParametersSerials();
		//Element[] array = new Element[2];
		ArrayList<Serializable> array = new ArrayList<Serializable>();
		ArrayList<Serializable> array2 = new ArrayList<Serializable>();
		Element el1 = new Element(), el2 = new Element(), el3, el4;
		el1.setCoordinate(1, 2);
		el2.setCoordinate(3, 4);
		array.add(el1);
		array.add(el2);
		info.addArrayParamData("ARRAY", array);
		try {
			array2 = (ArrayList<Serializable>) info.getArrayParam("ARRAY");
		} catch (InvalidPropertyException e) {
			// TODO Auto-generated catch block
			fail(e.getMessage());
		}
		el3 = (Element)array2.get(0);
		el4 = (Element)array2.get(1);
		assertArrayEquals(el1.toSerial().getBytes(), el3.toSerial().getBytes());
		assertArrayEquals(el2.toSerial().getBytes(), el4.toSerial().getBytes());
	}
	
}
