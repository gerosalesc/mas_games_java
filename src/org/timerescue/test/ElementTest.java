/**
 * 
 */
package org.timerescue.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.timerescue.action.ExecutionContext;
import org.timerescue.element.Coordinate;
import org.timerescue.element.Element;
import org.timerescue.exception.InvalidSerialException;

/**
 * @author chamanx
 *
 */
public class ElementTest {

	/**
	 * Test method for {@link org.timerescue.droid.element.Element#setCoordinate(org.timerescue.droid.element.Coordinate)}.
	 */
	@Test
	public void testSetCoordinate() {
		
		try {
			Element e = new Element();
			e.setCoordinate(6, 8);
			Coordinate c = new Coordinate(6,8);
			assertArrayEquals( c.toSerial().getBytes(), e.getCoordinate().toSerial().getBytes());
		} catch (Exception e) {
			fail("Exception: "+e.getMessage());
		}		
		
	}
	
	/**
	 * Test method for {@link org.timerescue.action.ExecutionContext#fromSerial(java.lang.String)}.
	 */
	@Test
	public void testFromSerial() {
		Element ec = new Element(); 
		Element ec2 = new Element();
		ec.setCoordinate(new Coordinate(1,2));
		ec2.setCoordinate(new Coordinate(3,4));
		try {
			String temp = ec.toSerial();
			ec2.fromSerial(temp);
		} catch (InvalidSerialException e) {
			fail(e.toString());
		}
		String temp1 = ec.toSerial();
		String temp2 = ec2.toSerial();
		assertArrayEquals(temp1.getBytes(), temp2.getBytes());
	}

}
