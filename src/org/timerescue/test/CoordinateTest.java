/**
 * 
 */
package org.timerescue.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.timerescue.action.ExecutionContext;
import org.timerescue.element.Coordinate;
import org.timerescue.exception.InvalidSerialException;

/**
 * @author chamanx
 *
 */
public class CoordinateTest {

	/**
	 * Test method for {@link org.timerescue.element.Coordinate#fromSerial(java.lang.String)}.
	 */
	@Test
	public void testFromSerial() {
		Coordinate ec = new Coordinate(1, 2);
		Coordinate ec2 = new Coordinate(3, 4);
		try {
			String temp = ec.toSerial();
			ec2.fromSerial(temp);
		} catch (InvalidSerialException e) {
			fail(e.toString());
		}
		String temp1 = ec.toSerial();
		String temp2 = ec2.toSerial();
		assertArrayEquals(temp1.getBytes(), temp2.getBytes());		
	}

}
