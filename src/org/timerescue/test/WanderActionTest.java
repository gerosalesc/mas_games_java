/**
 * 
 */
package org.timerescue.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.timerescue.action.WanderAction;
import org.timerescue.element.agent.Agent;

/**
 * @author chamanx
 *
 */
public class WanderActionTest {
	
	WanderAction action;
	Agent agent;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		action = new WanderAction();
		agent = new Agent();
		agent.setCoordinate(0, 0, 0);		
		action.addParamData("WANDERER", agent);
	}

	/**
	 * Test method for {@link org.timerescue.action.WanderAction#executeEnvironment()}.
	 */
	@Test
	public void testExecuteEnvironment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.timerescue.action.WanderAction#executeAgent()}.
	 */
	@Test
	public void testExecuteAgent() {
		agent.start();		
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.timerescue.action.WanderAction#executeOther()}.
	 */
	@Test
	public void testExecuteOther() {
		fail("Not yet implemented");
	}

}
