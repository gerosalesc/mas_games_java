/**
 * 
 */
package org.timerescue.data;

/**
 * This interface is meant to connect the objects with the meta data sources 
 * @author chamanx
 *
 */
public interface MetaDataLoader {
	
	/**
	 * Loads all agent meta data from a source
	 * 
	 */
	abstract void loadMetaData(MetaDataSource source);

}
