package org.timerescue.data;

/**
 * DataSource it's a class that contains information for access data
 * @author chamanx
 *
 */
public class MetaDataSource {
	
	/*
	 * Attributes
	 */
	/**
	 * String source that contains the connection information
	 */
	private String source;
	
	/*
	 * Methods
	 */
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
	

}
