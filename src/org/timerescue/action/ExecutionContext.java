package org.timerescue.action;

import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.Serializable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * Context describe the context in which an action may be executed
 * An example would be an environment Context, or a Agent's StateHolder Context
 * @author chamanx
 *
 */
public abstract class ExecutionContext implements Serializable{
	
	/*
	 * Attributes 
	 */
	/**
	 * This is the context name 
	 */
	private String context_name;
	//private ActionExecuter executer;
	
	/*
	 * Methods
	 */
	
	/**
	 * Sets Context_name property
	 * @param context_name
	 */
	protected void setContext_name(String context_name) {
		this.context_name = context_name;
	}
	
	/**
	 * Gets context_name property
	 * @return
	 */
	private String getContext_name() {
		return context_name;
	}
	
	/**
	 * Tells if a given Object Context is the same as this
	 * @param context object
	 * @return true if it is, false if not
	 */
	public boolean equals( ExecutionContext context ){
		return context.getContext_name().equals(this.getContext_name());
	}
	
	@Override
	public String toSerial() {
		JsonObject temp_serializer = new JsonObject();
		//Serialize name property
		temp_serializer.addProperty(Constants.NAME_PROPERTY, context_name);
//		//Serialize executer property
//		temp_serializer.addProperty(Constants.EXECUTER_PROPERTY, executer.toSerial());		
		return gson.toJson(temp_serializer);
	}
	
	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		JsonObject temp_deserializer;
		try {
			temp_deserializer = parser.parse(serialized).getAsJsonObject();			
			context_name = temp_deserializer.get(Constants.NAME_PROPERTY).getAsString();
//			executer.fromSerial(
//					temp_deserializer.get(Constants.EXECUTER_PROPERTY).getAsString());
		} catch (IllegalStateException e) {
			throw new InvalidSerialException(e);
		} catch (NullPointerException e) {
			throw new InvalidSerialException(e);
		} catch (JsonParseException e) {
			throw new InvalidSerialException(e);
		}
	}
	
	/*
	 * Static attributes
	 */
	public static class Constants{ 
		public static final String NAME_PROPERTY = "NAME";
		//public static final String EXECUTER_PROPERTY = "EXECUTER";
	}	
	
}
