package org.timerescue.action;

import org.timerescue.element.agent.Agent;
import org.timerescue.enviroment.Environment;
import org.timerescue.information.Serializable;
import org.timerescue.utils.Introspection;

public class ExecutionContextFactory {
	
	/*
	 * Methods
	 */
	/**
	 * Get an instance of an ExecutionContext of an Agent or Environment. 
	 * This may be overwritten for new contexts
	 * @param clas
	 * @return an instance 
	 * @throws InvalidContextSource 
	 */
	public static ExecutionContext getInstance(Class<?> clas) 
			throws InvalidContextSource{
		
		if(clas.equals(Agent.class)){
			return new AgentExecutionContext();
		}else if(clas.equals(Environment.class)){
			return new EnvironmentExecutionContext();
		}
		
		return null;
	}
}
