/**
 * This Class is used for Action unique recognition  
 * @author chamanx
 */
package org.timerescue.action;

public class ActionProperties {
	/**
	 * Atribs
	 */
	
	protected Integer id;
	protected Integer type;
	
	/**
	 * Static variables
	 */
	
	public final int TYPE_PASIVE = 0;
	public final int TYPE_ACTIVE = 1;
	
	/**
	 * Methods
	 */
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActionIdent [id=" + id + ", type=" + type + "]";
	}

	/**
	 * @return the id
	 */
	protected Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	protected Integer getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(Integer type) {
		this.type = type;
	}	
	
}
