/**
 * 
 */
package org.timerescue.action;


/**
 * @author chamanx
 *
 */
public class ActionFactory {
	
	/*
	 * Methods
	 */
	/**
	 * Get an instance of a given action class
	 * @param clas
	 * @return
	 */
	public static Action getInstance(Class<?> clas) {
		if(clas.equals(DefaultAttackAction.class)){
			return new DefaultAttackAction();
		}else if(clas.equals(WanderAction.class)){
			return new WanderAction();
		}
		return null;
	}
}
