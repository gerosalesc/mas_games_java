package org.timerescue.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.InformationParameters;
import org.timerescue.information.InformationParametersHash;
import org.timerescue.information.Serializable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

/**
 * @author chamanx
 */

public abstract class Action 
	extends InformationParametersHash{

	/*
	 * Attributes
	 */
	private ArrayList<ExecutionContext> contexts;
	

	/*
	 * Methods 
	 */	
	
	/**
	 * Default constructor
	 * @param parameters
	 */
	public Action() {
		super();
		//setParameters(parameters);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Add a new context. 
	 * This should be called only by specialized classes
	 * @param context
	 */
	protected void addContext(ExecutionContext context) {
		contexts.add(context);
	}
	
	/**
	 * Clear all execution contexts
	 */
	protected void clearContexts() {
		// TODO Auto-generated method stub
		contexts.clear();
	}
	
	/**
	 * Execute default action over the known parameters 
	 * @return
	 */
	public InformationParameters execute(ExecutionContext context){
		//Agent Execution Context
		if (context instanceof AgentExecutionContext){
			return executeAgent();
		} else if (context instanceof EnvironmentExecutionContext){
			return executeEnvironment();
		}else{ //Customized execution context
			return executeOther();
		}
	}
	
	/**
	 * Execute this action from environments contexts
	 * @return
	 */
	protected abstract InformationParameters executeEnvironment();
	
	/**
	 * Execute this action from agents' state holders contexts
	 * @return
	 */
	protected abstract InformationParameters executeAgent();
	
	/**
	 * Execute this action from customized contexts
	 * @return
	 */
	protected abstract InformationParameters executeOther();
	
	/**
	 * Remove states of from involved elements 
	 * @return
	 */
	public abstract boolean removeStates();
	
	@Override
	public String toSerial() {
		String serial = null;
		JsonObject temp_serializer = new JsonObject();
		JsonArray temp_contexts = new JsonArray();
		
		/*
		 * Get the parameters from the informationParameters super class
		 * then add it to the current serializer object
		 */		
		temp_serializer.add(
				Constants.PARAMETERS_PROPERTY, 
				new JsonPrimitive(super.toSerial()));
		//Serialize Contexts
		for (Iterator<ExecutionContext> iterator = contexts.iterator(); iterator.hasNext();) {
			ExecutionContext temp_context = iterator.next();
			temp_contexts.add(
					new JsonPrimitive(temp_context.toSerial()));
		}
		temp_serializer.add(Constants.CONTEXTS_PROPERTY, temp_contexts);
		serial = gson.toJson(temp_serializer);
		return serial;
	}
	
	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {				
		JsonObject temp_deserializer;
		JsonArray array;
		try {
			temp_deserializer = parser.parse(serialized).getAsJsonObject();
			//De-serialize parameters			
			super.fromSerial(
					temp_deserializer.get(
							Constants.PARAMETERS_PROPERTY).getAsString());
			//TODO De-serialize contexts	
//			array = temp_deserializer.get(Constants.CONTEXTS_PROPERTY).getAsJsonArray();
//			for (int i = 0; i < array.size(); i++) {
//				//Create a default context
//				EnvironmentExecutionContext temp_context = new EnvironmentExecutionContext();
//				temp_context.fromSerial(array.get(i).getAsString());
//				contexts.add(temp_context);
//			}
		} catch (IllegalStateException e) {
			throw new InvalidSerialException(e);
		} catch (NullPointerException e) {
			throw new InvalidSerialException(e);
		} catch (JsonParseException e) {
			throw new InvalidSerialException(e);
		}
	}
	
	/*
	 * Constants
	 */
	public static class Constants{
		public static final String PARAMETERS_PROPERTY = "PARAMETERS";
		public static final String CONTEXTS_PROPERTY = "CONTEXTS";
	}
}