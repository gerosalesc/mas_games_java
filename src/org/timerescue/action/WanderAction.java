/**
 * 
 */
package org.timerescue.action;

import java.util.Random;

import org.timerescue.element.Coordinate;
import org.timerescue.element.agent.Agent;
import org.timerescue.exception.InvalidPropertyException;
import org.timerescue.information.InformationParameters;

/**
 * An action that perform a random movement of some steps
 * @author chamanx
 *
 */
public class WanderAction extends Action {

	/* (non-Javadoc)
	 * @see org.timerescue.action.Action#executeEnvironment()
	 */
	@Override
	protected InformationParameters executeEnvironment() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.action.Action#executeAgent()
	 */
	@Override
	protected InformationParameters executeAgent() {
		// TODO Auto-generated method stub
		try {
			Random seed = new Random();
			Agent wanderer = (Agent)getParam(Constants.Parameter.WANDERER);
			int speed = Integer.parseInt(
					wanderer.getProperty_holder().getCurrentProperty(
							Agent.Constants.Property.SPEED));
			//points toward a random direction
			wanderer.move(
					new Coordinate(seed.nextInt(), seed.nextInt(), seed.nextInt()),
					speed);
		} catch (InvalidPropertyException e) {
			// TODO log this
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.action.Action#executeOther()
	 */
	@Override
	protected InformationParameters executeOther() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Constants
	 */
	public static class Constants{
		/**
		 * Default values 
		 */
		public static class Default{
			
		}
		/**
		 * Parameters that are expected to be included for this action
		 */
		public static class Parameter{			
			/**
			 * This is the agent which must execute the action
			 */
			public static final String WANDERER = "WANDERER";
		}
	}

	@Override
	public boolean removeStates() {
		// TODO Auto-generated method stub
		return true;
	}
}
