/**
 * 
 */
package org.timerescue.action;

import org.timerescue.enviroment.Environment;


/**
 * @author chamanx
 *
 */
public class EnvironmentExecutionContext extends ExecutionContext {
	
	/*
	 * Methods
	 */	
	public EnvironmentExecutionContext() {
		super();
		super.setContext_name(Environment.class.getName());
	}

}
