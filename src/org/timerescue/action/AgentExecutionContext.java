/**
 * 
 */
package org.timerescue.action;

import org.timerescue.element.agent.Agent;

/**
 * @author chamanx
 *
 */
public class AgentExecutionContext extends ExecutionContext {
	
	/*
	 * Methods
	 */
	
	public AgentExecutionContext() {
		super();
		super.setContext_name(Agent.class.getName());
	}
	
}
