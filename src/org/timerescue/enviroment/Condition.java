/**
 * 
 */
package org.timerescue.enviroment;

import java.util.Vector;

import org.timerescue.action.Action;

/**
 * Class condition contains a set of Actions to execute over agents
 * at it's injection time.
 * @author chamanx
 *
 */
public class Condition {
	
	/*
	 * Attributes
	 */
	private Vector<Action> conditions;
	
	/*
	 * Methods
	 */
	
	/**
	 * @return the conditions
	 */
	public Vector<Action> getConditions() {
		return conditions;
	}

	/**
	 * @param conditions the conditions to set
	 */
	public void addCondition(Action condition) {
		this.conditions.add(condition);
	}
	
	
}
