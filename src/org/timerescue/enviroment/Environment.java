/**
 * 
 */
package org.timerescue.enviroment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.timerescue.action.Action;
import org.timerescue.action.ExecutionContext;
import org.timerescue.action.ExecutionContextFactory;
import org.timerescue.action.InvalidContextSource;
import org.timerescue.element.Coordinate;
import org.timerescue.element.Element;
import org.timerescue.element.agent.Agent;
import org.timerescue.exception.InvalidSerialException;
import org.timerescue.information.InformationParameters;
import org.timerescue.information.InformationParametersHash;
import org.timerescue.information.Serializable;

/**
 * Environment class 
 * @author chamanx
 *
 */
public class Environment implements ActionExecuter{
	
	/*
	 * Attributes
	 */
	
	//private Condition condition;
	private Vector<Action> conditions;
	private Vector<Agent> agents;
	private Vector<Element> elements;
	private ExecutionContext context;
	
	/*
	 * Methods
	 */
	
	//@Inject
	/**
	 * Default constructor
	 */
	public Environment() {
		try {
			this.context = ExecutionContextFactory.getInstance(Environment.class);
		} catch (InvalidContextSource e) {
			// TODO Log this			
		}
		applyConditions();
	}
	
	/**
	 *  Run environment
	 */
	public void run() {
		// TODO Auto-generated method stub
		//See what all agents are up to
		for (Iterator<Agent> iterator = agents.iterator(); iterator.hasNext();) {
			Agent agent =  iterator.next();
			//Sets environment information for all Agents
			//TODO Change the default neighbor radio for the distance perception of the agent			
			sendParametersToAgent(
					getSorroundings(
						agent.getCoordinate(), 
						Coordinate.Constants.DEFAULT_NEIGHBOR_RADIO), agent);
			//Ask the agent what to do
			Action action = agent.decide();
			//Executes the action
			executeAction(action);
			
		}

	}
	
	/**
	 * Get all elements and agents near a point
	 * @param center is the reference point
	 * @param distance_sense is the distance sensed by a given element 
	 * in the center position
	 * @return An Information parameter object with all the elements
	 */
	private InformationParameters getSorroundings(
			Coordinate center, 
			int distance_sense) {
		// TODO Optimization for this
		InformationParameters information_paramaters = new InformationParametersHash();
		List<Serializable> data_elements = new ArrayList<Serializable>();
		//Sent all closer Elements in the map
		for (Iterator<Element> iterator = elements.iterator(); iterator.hasNext();) {
			Element element =  iterator.next();
			
			if (center.isClose(element.getCoordinate(), distance_sense)){
				//information_paramaters.addParamData(Constants.SURROUND_PROPERTY, element);
				
			}
		}
		//Sent all closer Agents in the map
		for (Iterator<Agent> iterator = agents.iterator(); iterator.hasNext();) {
			Agent agent =  iterator.next();
			
			if (center.isClose(agent.getCoordinate(), distance_sense)){
				information_paramaters.addParamData(Constants.SURROUND_PROPERTY, agent);
			}
		}
		//Add the array to the parameters object
		information_paramaters.addArrayParamData(
				Constants.SURROUND_PROPERTY, 
				data_elements);
		return information_paramaters;
	}
	
	/**
	 * Sends an Information parameters to an Agent to be executed
	 * @param parameters
	 * @param agent
	 */
	private void sendParametersToAgent(InformationParameters parameters, Agent agent) {
		agent.setSorroundings(parameters);				
	}
	
	/**
	 * Send all enviroment's conditions to agents
	 */
	private void applyConditions() {
		//Conditions Loop
		for (Iterator<Action> iterator = conditions.iterator(); iterator.hasNext();) {
			Action condition = iterator.next();
			//Agents Loop
			for (Iterator<Agent> agent_iterator = agents.iterator(); 
					agent_iterator.hasNext();) {
				Agent agent = agent_iterator.next();
				sendParametersToAgent(condition, agent);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.timerescue.droid.enviroment.ActionExecutener#executeAction(org.timerescue.driod.action.Action)
	 */
	@Override
	public void executeAction(Action action) {
		if(action != null){
			action.execute(context);
		}
	}
	
	/*
	 * Constants
	 */
	public static class Constants{
		public static final String SURROUND_PROPERTY = "SURROUND";
	}

	@Override
	public String toSerial() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		// TODO Auto-generated method stub
		
	}
}
