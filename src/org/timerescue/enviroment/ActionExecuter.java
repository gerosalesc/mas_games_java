/**
 * 
 */
package org.timerescue.enviroment;

import org.timerescue.action.Action;
import org.timerescue.information.Serializable;

/**
 * @author chamanx
 *
 */
public interface ActionExecuter extends Serializable {

	/**
	 * This method must implements a logic to execute all type of Action objects received as 
	 * parameter and as well notify all elements affected by this action 
	 * @param action
	 */
	public void executeAction(Action action);

}
