/**
 * 
 */
package org.timerescue.exception;

/**
 * @author chamanx
 *
 */
public class InvalidSerialException extends Throwable {
	
	/*
	 * Attributes
	 */
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * Methods
	 */
	/**
	 * 
	 * @param e
	 */
	public InvalidSerialException( Throwable e) {
		super();
		
	}
}
