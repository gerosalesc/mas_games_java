/**
 * 
 */
package org.timerescue.information;

import org.timerescue.exception.InvalidSerialException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * This class is a local serializable replacement for the java.lang.String 
 * that may be used for all types, primitive and others objects that 
 * doesn't implement org.timerescue.information.Serializable and only have 
 * single values to be included as one
 * @author chamanx
 *
 */
public class StringSerial implements Serializable {
	
	/*
	 * Attributes
	 */
	private String value;
	
	/*
	 * Methods 
	 */
	/**
	 * Default constructor
	 */
	public StringSerial() {
		value = "";
	}
	
	public StringSerial(String value) {
		setValue(value);
	}
	/**
	 * @return the java.lang.String representation
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.Serializable#toSerial()
	 */
	@Override
	public String toSerial() {
		// TODO Auto-generated method stub
		JsonObject serializer = new JsonObject();
		serializer.addProperty(Constants.VALUE_PROPERTY, this.value);
		return gson.toJson(serializer);
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.Serializable#fromSerial(org.timerescue.information.String)
	 */
	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		// TODO Auto-generated method stub
		try{
			this.value = parser.parse(serialized).getAsJsonObject().get(
					Constants.VALUE_PROPERTY).getAsString();
		} catch (IllegalStateException e) {
			throw new InvalidSerialException(e);
		} catch (NullPointerException e) {
			throw new InvalidSerialException(e);
		} catch (JsonParseException e) {
			throw new InvalidSerialException(e);
		}
	}
	
	/*
	 * Constants
	 */
	public static class Constants{
		public static final String VALUE_PROPERTY = "VALUE";
	}
}
