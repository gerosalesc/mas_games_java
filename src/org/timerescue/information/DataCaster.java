/**
 * 
 */
package org.timerescue.information;

import org.timerescue.action.Action;
import org.timerescue.element.Element;
import org.timerescue.element.agent.Agent;

/**
 * This interface define methods which every parameter
 * object must implement in order to return expected information
 * of any type, this services are reusable for anyone that
 * works with any type supported
 * @author chamanx
 *
 */
public interface DataCaster {
	
	/**
	 * Return raw object with the info  
	 * @return
	 */
	Object getInfoObject();
	
	/**
	 * Return Element object with the data  
	 * @return
	 */
	Element getInfoElement() throws ClassCastException;
	
	/**
	 * Return Agent object with the data  
	 * @return
	 */
	Agent getInfoAgent() throws ClassCastException;
	
	/**
	 * Return Information object with the data  
	 * @return
	 */
	InformationParametersSerials getInfoInformation() throws ClassCastException;
	
	/**
	 * Return Action object with the data  
	 * @return
	 */
	Action getInfoAction() throws ClassCastException;
	
}
