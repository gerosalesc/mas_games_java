/**
 * 
 */
package org.timerescue.information;

import org.timerescue.exception.InvalidSerialException;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

/**
 * This interface define methods for the serializable objects
 * @author chamanx
 *
 */
public interface Serializable {
	
	/**
	 * default serialization objects
	 */
	Gson gson = new Gson();
	JsonParser parser = new JsonParser();
	
	/**
	 * This method should serialize an object
	 * @return the String representing the object
	 */
	String toSerial();
	

	/**
	 * Creates a new object by reading the properties from a serialized object string
	 * The object needs to be casted back to its original form afterwards
	 * @param serialized string that contains the object data
	 * @return The generic Object with the data
	 * @throws InvalidSerialException
	 */
	void fromSerial(String serialized) throws InvalidSerialException;
	
}
