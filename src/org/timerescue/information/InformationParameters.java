/**
 * 
 */
package org.timerescue.information;

import java.util.List;

import org.timerescue.exception.InvalidPropertyException;
import org.timerescue.exception.InvalidSerialException;

/**
 * @author chamanx
 *
 */
public interface InformationParameters extends Serializable {

	/*
	 * Methods
	 */	
	
	/**
	 * @return the size
	 */
	public int getSize();
	
	/**
	 * Return an serializable object parameter data corresponding the specified property
	 * @param property
	 * @throws InvalidSerialException 
	 * @throws InvalidPropertyException
	 * @return a serializable object, copy of the former serialized one  
	 */
	public Serializable getParam(String property) 
			throws InvalidPropertyException;
	
	/**
	 * Return an serializable object parameter array corresponding the specified property
	 * @param property
	 * @throws InvalidSerialException 
	 * @throws InvalidPropertyException
	 * @return a serializable object, copy of the former serialized one  
	 */
	public List<Serializable> getArrayParam(String property) 
			throws InvalidPropertyException;

	/**
	 * Serialize and Add a new parameter. 
	 * 
	 * 
	 * @param data_object is a serializable object
	 * @throws InvalidSerialException 
	 */
	public void addParamData(String property, Serializable data_object);
	
	/**
	 * Serialize and Add a new array with parameters objects.
	 * @param property
	 * @param data_objects
	 */
	public void addArrayParamData(String property, List<Serializable> data_objects) ;
			
	/**
	 * Removes all parameters data
	 */
	public void clear() ;
}
