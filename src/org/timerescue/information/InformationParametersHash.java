/**
 * 
 */
package org.timerescue.information;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.timerescue.exception.InvalidPropertyException;
import org.timerescue.exception.InvalidSerialException;

/**
 * @author chamanx
 *
 */
public class InformationParametersHash implements InformationParameters {
	
	/*
	 * Attributes
	 */
	/**
	 * All the objects
	 */
	private HashMap<String, List<Serializable>> parameters;
	
	/*
	 * Methods
	 */	
	//@Inject
	/**
	 * Default constructor
	 */
	public InformationParametersHash() {
		//Initialization
		this.parameters = new HashMap<String, List<Serializable>>();
	}
	
	/**
	 * Set the parameters
	 */
	protected void setParameters(HashMap<String, List<Serializable>> parameters){
		this.parameters = parameters;
	}
	
	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#getSize()
	 */
	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return parameters.size();
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#getParam(java.lang.String)
	 */
	@Override
	public Serializable getParam(String property)
			throws InvalidPropertyException {
		// TODO Auto-generated method stub
		List<Serializable> params;
		Serializable param = null;
		params = parameters.get(property);
		//Check if it is a List don't do anything
		if(params.size() > 1){
			//TODO log this
		}else{
			param = params.get(0);
		}
		return param;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#getArrayParam(java.lang.String)
	 */
	@Override
	public List<Serializable> getArrayParam(String property)
			throws InvalidPropertyException {
		List<Serializable> params;
		params = parameters.get(property);
		return params;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#addParamData(java.lang.String, org.timerescue.information.Serializable)
	 */
	@Override
	public void addParamData(String property, Serializable data_object) {
		ArrayList<Serializable> container = new ArrayList<Serializable>();
		container.add(data_object);
		parameters.put(property, container);
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#addArrayParamData(java.lang.String, org.timerescue.information.Serializable[])
	 */
	@Override
	public void addArrayParamData(String property, List<Serializable> data_objects) {
		parameters.put(property, data_objects);
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.InformationParameters#clear()
	 */
	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}
	
	/* (non-Javadoc)
	 * @see org.timerescue.information.Serializable#toSerial()
	 */
	@Override
	public String toSerial() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.timerescue.information.Serializable#fromSerial(java.lang.String)
	 */
	@Override
	public void fromSerial(String serialized) throws InvalidSerialException {
		// TODO Auto-generated method stub

	}

}
